## Weather API App

### Library Use

* Butterknife  - this is for easily handling the views from activity
* Retrofit - this is use for handling REST API request from server
* Picasso - this is use for handling iamges cache
* Maps SDK - this is use to display location of the country
* Parceler - this is use for serializing data for sharing object on another activity

##### Tools

* Android Studio
* PojoGson converter
* Postman
* Bitbucket

##### Implementation

* Using MVP as main design architecture
* Uses fragment for handling views of maps.


##### Notes

* I use "qa" as flavor rather than "test" because Android Studio didn't accept "test" for naming convention.
* I provided release apk for dev on this repo named "app-dev-release.apk"
* I provided release apk for production on this repo named "app-production-release.apk"
 