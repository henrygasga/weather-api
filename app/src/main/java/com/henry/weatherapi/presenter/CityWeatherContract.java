package com.henry.weatherapi.presenter;

import android.content.Context;

import com.henry.weatherapi.model.CityWeatherParams;
import com.henry.weatherapi.model.Country;
import com.henry.weatherapi.model.ListModel;

import java.util.List;

public interface CityWeatherContract {

    interface View {
        Context getContext();
        void onCountrySuccess(Country country);
        void onCityWeatherSuccess(List<ListModel> listModels);
        void onError(String message);
    }

    interface Presenter {
        void getCityWeather(CityWeatherParams cityWeatherParams);
        void getCountry(CityWeatherParams cityWeatherParams);
    }
}
