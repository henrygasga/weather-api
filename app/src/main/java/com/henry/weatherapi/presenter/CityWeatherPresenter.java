package com.henry.weatherapi.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.henry.weatherapi.Constant;
import com.henry.weatherapi.api.ApiService;
import com.henry.weatherapi.model.CityWeatherParams;
import com.henry.weatherapi.model.Country;
import com.henry.weatherapi.model.ListModel;
import com.henry.weatherapi.model.ResponseModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CityWeatherPresenter implements CityWeatherContract.Presenter {

    private static Retrofit retrofit = null;
    private CityWeatherContract.View view;

    public CityWeatherPresenter(CityWeatherContract.View view) {
        this.view = view;
    }

    @Override
    public void getCityWeather(CityWeatherParams cityWeatherParams) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ApiService apiService = retrofit.create(ApiService.class);
        Call<ResponseModel> call = apiService.getCityWeather(
                cityWeatherParams.getLat(),
                cityWeatherParams.getLon(),
                cityWeatherParams.getCnt(),
                cityWeatherParams.getAppid()
        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(Integer.valueOf(response.body().getCod()) == 200) {
                    Type listType = new TypeToken<ArrayList<ListModel>>() {}.getType();
                    ArrayList<ListModel> items = new Gson().fromJson(response.body().getList(), listType);
                    view.onCityWeatherSuccess(items);
                } else {
                    view.onError(response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable throwable) {
                Log.e("CityWeatherPresenter", throwable.toString());
            }
        });
    }

    @Override
    public void getCountry(CityWeatherParams cityWeatherParams) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ApiService apiService = retrofit.create(ApiService.class);
        Call<Country> call = apiService.getCountry(
                cityWeatherParams.getCountry(),
                cityWeatherParams.getAppid()
        );

        call.enqueue(new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if(200 == response.body().getCod()) {
                    Country country = response.body();
                    view.onCountrySuccess(country);
                } else {
                    view.onError("Failed to get response.");
                }

            }

            @Override
            public void onFailure(Call<Country> call, Throwable throwable) {
                Log.e("CityWeatherPresenter", throwable.toString());
            }
        });
    }
}
