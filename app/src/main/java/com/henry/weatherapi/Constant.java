package com.henry.weatherapi;

public class Constant {

    public static final String APP_ID = "32e9d58bb392aba86132f9c80d343a40";
    public static final String BASE_URL = "http://api.openweathermap.org";
    public static final String IMG_URL = "http://openweathermap.org/img/w/";

    public static final Double LONDON_LAT = 51.51;
    public static final Double LONDON_LON = -0.13;

    public static final String LONDON = "London";
    public static final String PRAGUE = "Prague";
    public static final String SAN_FRANCISCO = "San Francisco";

    public static final String FOR_WEATHER_DETAILS = "FOR_WEATHER_DETAILS";
}
