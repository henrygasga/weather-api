package com.henry.weatherapi.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.henry.weatherapi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryFragment extends Fragment implements OnMapReadyCallback {

    private static final String COUNTRYNAME = "countryname";
    private static final String WEATHER = "weather";
    private static final String LAT = "lat";
    private static final String LON = "lon";

    private String countryName;
    private String weather;
    private String lat;
    private String lon;

    MapView mapView;
    private GoogleMap mMap;

    @BindView(R.id.tv1) TextView tv1;
    @BindView(R.id.tv2) TextView tv2;

    public static CountryFragment newInstance(String countryName, String weather, String lat, String lon) {
        CountryFragment fragment = new CountryFragment();
        Bundle args = new Bundle();
        args.putString(COUNTRYNAME, countryName);
        args.putString(WEATHER, weather);
        args.putString(LAT, lat);
        args.putString(LON, lon);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            countryName = getArguments().getString(COUNTRYNAME);
            weather = getArguments().getString(WEATHER);
            lat = getArguments().getString(LAT);
            lon = getArguments().getString(LON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, rootView);
        mapView = (MapView) rootView.findViewById(R.id.map);

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

        tv1.setText(countryName);
        tv2.setText(weather);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity().getApplicationContext());
        mMap = googleMap;
        Marker marker;

        if (mMap != null) {
            mMap.getUiSettings().setAllGesturesEnabled(false);
            GoogleMapOptions options = new GoogleMapOptions().liteMode(true);

            LatLng mapPointer = new LatLng(Double.valueOf(lat), Double.valueOf(lon));
            marker = mMap.addMarker(new MarkerOptions()
                    .position(mapPointer).title(countryName)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                    .draggable(false).visible(true));

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 14));
        }
    }
}
