package com.henry.weatherapi.view;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.henry.weatherapi.Constant;
import com.henry.weatherapi.R;
import com.henry.weatherapi.model.Country;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherDetailActivity extends AppCompatActivity {

    private Country country;

    @BindView(R.id.my_toolbar) Toolbar toolbar;
    @BindView(R.id.tv_country) TextView tvCountry;
    @BindView(R.id.tv_temp) TextView tvTemp;
    @BindView(R.id.tv_details) TextView tvDetails;
    @BindView(R.id.imgIcon) ImageView imgIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        country = Parcels.unwrap(getIntent().getParcelableExtra(Constant.FOR_WEATHER_DETAILS));

        tvCountry.setText(country.getName() + ", " +country.getSys().getCountry());

        tvDetails.setText(country.getWeather().get(0).getDescription().toUpperCase(Locale.US) +
        "\n Humidity: " + country.getMain().getHumidity() +
        "\n Pressure: " + country.getMain().getPressure());

        tvTemp.setText(String.format("%.2f", country.getMain().getTemp())+ " ℃");

        Picasso.with(getApplicationContext())
                .load(Constant.IMG_URL + country.getWeather().get(0).getIcon() + ".png")
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .fit()
                .into(imgIcon);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
