package com.henry.weatherapi.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.henry.weatherapi.R;
import com.henry.weatherapi.view.fragment.MainScreen;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.my_toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

//        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_content,
//                new MainScreen()).addToBackStack(null).commit();

    }
}
