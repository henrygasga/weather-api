package com.henry.weatherapi.view.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.henry.weatherapi.Constant;
import com.henry.weatherapi.R;
import com.henry.weatherapi.adapter.ResponseAdapter;
import com.henry.weatherapi.model.CityWeatherParams;
import com.henry.weatherapi.model.Country;
import com.henry.weatherapi.model.ListModel;
import com.henry.weatherapi.presenter.CityWeatherContract;
import com.henry.weatherapi.presenter.CityWeatherPresenter;
import com.henry.weatherapi.view.WeatherDetailActivity;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainScreen extends AppCompatActivity implements CityWeatherContract.View, SwipeRefreshLayout.OnRefreshListener {

    protected CityWeatherContract.Presenter presenter;
    private List<Country> itemsList = null;
    private ResponseAdapter<Country> adapter;
    CityWeatherParams params;

    @BindView(R.id.swipe_container) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.first_frame) FrameLayout firstFrame;
    @BindView(R.id.sec_frame) FrameLayout secFrame;
    @BindView(R.id.third_frame) FrameLayout thirdFrame;
    @BindView(R.id.my_toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_screen);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        presenter = new CityWeatherPresenter(this);
        params = new CityWeatherParams();
        itemsList = new ArrayList<>();
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);
                requestDataFromServer(Constant.LONDON);
            }
        });
    }

    public void requestDataFromServer(String country) {
        params.setCountry(country);
        if(country.equalsIgnoreCase(Constant.LONDON)) {
            if (itemsList != null) {
                itemsList.clear();
            }
        }
        presenter.getCountry(params);
    }

    @OnClick(R.id.btn_refresh)
    public void refreshFragment() {
        swipeRefreshLayout.setRefreshing(true);
        requestDataFromServer(Constant.LONDON);
    }

    @OnClick({R.id.first_frame,R.id.sec_frame,R.id.third_frame})
    public void showDetails(View v) {
        Intent intent = new Intent(MainScreen.this, WeatherDetailActivity.class);
        Bundle bundle = new Bundle();
        switch (v.getId()){
            case R.id.first_frame:
                bundle.putParcelable(Constant.FOR_WEATHER_DETAILS, Parcels.wrap(itemsList.get(0)));
                break;
            case R.id.sec_frame:
                bundle.putParcelable(Constant.FOR_WEATHER_DETAILS, Parcels.wrap(itemsList.get(1)));
                break;
            case R.id.third_frame:
                bundle.putParcelable(Constant.FOR_WEATHER_DETAILS, Parcels.wrap(itemsList.get(2)));
                break;
        }
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        requestDataFromServer(Constant.LONDON);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onCountrySuccess(Country country) {
        int containerId = 0;
        String cnt = "";
        boolean loadNext = true;

        if(params.getCountry().equalsIgnoreCase(Constant.LONDON)) {
            if (itemsList != null) {
                itemsList.clear();
            }
        }
        itemsList.add(country);

        switch (params.getCountry()) {
            case Constant.LONDON:
                containerId = R.id.first_frame;
                cnt = Constant.PRAGUE;
                break;
            case Constant.PRAGUE:
                containerId = R.id.sec_frame;
                cnt = Constant.SAN_FRANCISCO;
                break;
            case Constant.SAN_FRANCISCO:
                containerId = R.id.third_frame;
                loadNext = false;
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(containerId,
                CountryFragment.newInstance(
                        country.getName(),
                        country.getWeather().get(0).getMain() + " - " + country.getWeather().get(0).getDescription() + " - " + country.getMain().getTemp() + " C",
                        String.valueOf(country.getCoord().getLat()),
                        String.valueOf(country.getCoord().getLon())
                )).addToBackStack(null).commit();

        if(loadNext) {
            requestDataFromServer(cnt);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onCityWeatherSuccess(List<ListModel> listModels) {

    }

    @Override
    public void onError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
