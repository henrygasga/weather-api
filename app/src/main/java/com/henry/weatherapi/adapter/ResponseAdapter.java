package com.henry.weatherapi.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.henry.weatherapi.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ResponseAdapter<T> extends RecyclerView.Adapter<ResponseAdapter.BindingHolder> {

    private List<T> items;
    private int layoutId;
    private int brId;
    private Context context;
    LatLng p1 = null;

    public class BindingHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        private ViewDataBinding binding;
        MapView mapView;
        private GoogleMap mMap;
        T item;

        public BindingHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
            mapView = (MapView) v.findViewById(R.id.map);
//            imageview_child_item = (ImageView) v.findViewById(R.id.imageview_child_item);
        }

        public void initializeMapView(Double lat, Double lon) {
            if (mapView != null) {
                mapView.onCreate(null);
                mapView.onResume();
                mapView.getMapAsync(this);
            }
            p1 = new LatLng(lat,lon);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context.getApplicationContext());
            mMap = googleMap;

            mMap.getUiSettings().setAllGesturesEnabled(false);
            GoogleMapOptions options = new GoogleMapOptions().liteMode(true);

            mMap.addMarker(new MarkerOptions().position(p1).title("test"));
        }
    }

    public ResponseAdapter(List<T> items, int layoutId, int brId, Context context) {
        this.items = new ArrayList<>(items);
        this.layoutId = layoutId;
        this.brId = brId;
        this.context = context;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);

        BindingHolder holder = new BindingHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ResponseAdapter.BindingHolder holder, int position) {
        T item = items.get(position);
        holder.getBinding().setVariable(brId, item);
        holder.initializeMapView(51.51,-0.13);

        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void notifyDataSetChanged(List<T> item) {
        this.items = item;
        notifyDataSetChanged();
    }
}
