package com.henry.weatherapi.api;

import com.henry.weatherapi.model.Country;
import com.henry.weatherapi.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/data/2.5/find?lat={lat}&lon={lon}&cnt={cnt}&APPID={appid}")
    Call<ResponseModel> getCityWeather(
            @Path("lat") Double lat,
            @Path("lon") Double lon,
            @Path("cnt") int cnt,
            @Path("appid") String appid
    );

    @GET("/data/2.5/weather")
    Call<Country> getCountry(
            @Query("q") String country,
            @Query("APPID") String appid
    );

}
