package com.henry.weatherapi.model;

import com.henry.weatherapi.Constant;

import org.parceler.Parcel;

@Parcel
public class CityWeatherParams {

    private Double lat = 0.0;
    private Double lon = 0.0;
    private int cnt = 5;
    private String appid = Constant.APP_ID;
    private String country = "";

    public CityWeatherParams() {
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
